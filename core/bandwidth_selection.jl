
function select_bandwidth!(d, l_bound::Float64, u_bound::Float64, nbr_points::Int; 
                           flag_save_results = false, path_results = "./")
    d.bandwidth = select_bandwidth(d, l_bound, u_bound, nbr_points; 
                                   flag_save_results = flag_save_results, path_results = path_results)
end

optimal_bandwidth_gaussian_1d(nbr_observations::Int, std_observations::Float64) =
    1.06*std_observations*nbr_observations^(-1/5)
asymptotic_bandwidth_gaussian_multidim(dim_observations::Int, nbr_observations::Int) = 
    (4/(dim_observations+2))^(1/(dim_observations+4))*nbr_observations^(-1/(dim_observations+4))
asymptotic_bandwidth_beta(dim_observations::Int, nbr_observations::Int) = nbr_observations^(-2/(dim_observations+4))

## Univariate distributions

# Least-square cross validation
function lscv(d::UnivariateKDE, bw::Float64; maxevals::Int = typemax(Int), verbose = false)
    res_ise = ise_kde(d, bw; maxevals = maxevals) 
    res_loo = loo_kde(d, bw)
    res_lscv = (res_ise - res_loo)
    if verbose
        println("Bandwidth: $bw, LSCV: $res_lscv, square integral kde: $res_ise, loo: $res_loo")
    end
    return res_lscv
end

# Leave-one out cv estimator
# Estimation of 2 * integral of \hat{π}_h * π  by leave-one out CV
function loo_kde(d::UnivariateKDE, bw::Float64)
    res_loo = 0.0
    for i = 1:d.nbr_obs
        res_loo += summand_loo_kde(d, i, bw)
    end
    return (2/d.nbr_obs)*res_loo
end
# Summand of leave-one-out estimator
# Compute \hat{π}^{(-i)}_h (x_i)
function summand_loo_kde(d::UnivariateKDE, i::Int, bw::Float64)
    if d.kernel == "chen99"
        return _summand_loo_kde_univariate_chen99(d.observations_scaled, d.weights, 
                                                 d.lower_bound, d.upper_bound, i, bw)
    elseif d.kernel == "gaussian"
        return _summand_loo_kde_univariate_gaussian(d.observations_scaled, d.weights, i, bw)
    else
        return missing
    end
end
function _summand_loo_kde_univariate_chen99(observations_scaled::Vector{Float64}, weights::Vector{Float64}, 
                                           lower_bound::Float64, upper_bound::Float64, 
                                           i::Int, bw::Float64)
    l_res = zeros(Threads.nthreads())
    l_norm_weights = zeros(Threads.nthreads())
    nbr_obs = length(observations_scaled)
    Threads.@threads for j = 1:nbr_obs
        if (j == i) continue end
        th_id = Threads.threadid()
        l_res[th_id] += weights[j] * beta_integrande(observations_scaled[j], observations_scaled[i], bw)
        l_norm_weights[th_id] += weights[j]
    end
    norm_weights = sum(l_norm_weights)
    res = sum(l_res)
    cte_denom = (upper_bound - lower_bound) * norm_weights *
                 beta(observations_scaled[i]/bw + 1, (1-observations_scaled[i])/bw + 1)
    return res / cte_denom
end
function _summand_loo_kde_univariate_gaussian(observations_scaled::Vector{Float64}, weights::Vector{Float64}, 
                                             i::Int, bw::Float64)
    l_res = zeros(Threads.nthreads())
    l_norm_weights = zeros(Threads.nthreads())
    nbr_obs = length(observations_scaled)
    Threads.@threads for j = 1:nbr_obs
        if (j == i) continue end
        th_id = Threads.threadid()
        l_res[th_id] += weights[j] * gaussian_kernel(observations_scaled[j], observations_scaled[i], bw)
        l_norm_weights[th_id] += weights[j]
    end
    norm_weights = sum(l_norm_weights)
    res = sum(l_res)
    cte_denom = bw * norm_weights
    return res / cte_denom
end

# Integral square error
function ise_kde(d::UnivariateKDE, bw::Float64; maxevals::Int = typemax(Int))
    if d.kernel == "chen99"
        return _ise_quadgk_univariate_chen99(d.log_observations_scaled, d.log_1_observations_scaled, 
                                             d.weights, d.lower_bound, d.upper_bound, bw; maxevals = maxevals)
    elseif d.kernel == "gaussian"
        return _ise_univariate_gaussian(d.observations, d.weights, bw)
    else
        return missing
    end
end
# Integral square error estimation of beta kernel with numerical integration
#function _integrande_ise_univariate_kk(x::Float64, observations_scaled_k::Float64, bw::Float64)
#    return beta_integrande(observations_scaled_k, x, bw)^2 / beta(x/bw + 1, (1-x)/bw + 1)^2
#end
#=
function _integrande_ise_univariate_ij(x::Float64, observations_scaled_i::Float64, observations_scaled_j::Float64, bw::Float64)
    return (beta_integrande(observations_scaled_i, x, bw) *
            beta_integrande(observations_scaled_j, x, bw)) /
            beta(x/bw + 1, (1-x)/bw + 1)^2 
end
=#
function _integrande_ise_paired_univariate(x::Float64, 
                                           log_obs_scaled::AbstractVector{Float64}, 
                                           log_1_obs_scaled::AbstractVector{Float64}, 
                                           weights::Vector{Float64}, 
                                           bw::Float64)
    res = 0.0
    a, b = x/bw, (1-x)/bw
    for k = eachindex(weights)
        #@inbounds res += (weights[k] * beta_integrande(observations_scaled[k], x, bw))^2
        @inbounds res += (weights[k] * beta_integrande(a, b, log_obs_scaled[k], log_1_obs_scaled[k]))^2
    end
    res /=  beta(a+1, b+1)^2
    return res
end
function _integrande_ise_unpaired_univariate(x::Float64, 
                                             log_obs_scaled::AbstractVector{Float64}, 
                                             log_1_obs_scaled::AbstractVector{Float64}, 
                                             weights::Vector{Float64}, 
                                             bw::Float64)
    nbr_obs = length(log_obs_scaled)
    res = 0.0
    a, b = x/bw, (1-x)/bw
    for i = eachindex(weights)
        l_res_i = zeros(Threads.nthreads())
        Threads.@threads for j = (i+1):nbr_obs
            #@inbounds l_res_i[Threads.threadid()] += weights[j] * beta_integrande(observations_scaled[j], x, bw)
            @inbounds l_res_i[Threads.threadid()] += weights[j] * beta_integrande(a, b, log_obs_scaled[j], log_1_obs_scaled[j])
        end
        #res_i = 2 * weights[i] * beta_integrande(observations_scaled[i], x, bw) * sum(l_res_i)
        res_i = 2 * weights[i] * beta_integrande(a, b, log_obs_scaled[i], log_1_obs_scaled[i]) * sum(l_res_i)
        res += res_i
    end
    res /=  beta(a+1, b+1)^2
    return res
end 
# Estimation of integral of \hat{π}_h^2 by numerical integration
function _ise_quadgk_univariate_chen99(log_obs_scaled::Vector{Float64}, log_1_obs_scaled::Vector{Float64},
                                       weights::Vector{Float64}, 
                                       lower_bound::Float64, upper_bound::Float64, 
                                       bw::Float64; maxevals::Int = typemax(Int))
    f_int_paired(x) = _integrande_ise_paired_univariate(x, log_obs_scaled, log_1_obs_scaled, weights, bw)
    integral_paired, err_paired = quadgk(f_int_paired, 0, 1, rtol=1e-6, maxevals = maxevals)
    f_int_unpaired(x) = _integrande_ise_unpaired_univariate(x, log_obs_scaled, log_1_obs_scaled, weights, bw)
    integral_unpaired, err_unpaired = quadgk(f_int_unpaired, 0, 1, rtol=1e-6, maxevals = maxevals)
    
    int_kde_sq = integral_paired + integral_unpaired
    int_kde_sq /= prod(upper_bound - lower_bound)

    return int_kde_sq
end
#=
function _ise_quadgk_univariate_chen99_bis(observations_scaled::Vector{Float64}, weights::Vector{Float64}, 
                                       lower_bound::Float64, upper_bound::Float64, bw::Float64)
    nbr_obs = length(observations_scaled)
    int_kde_square = zeros(Threads.nthreads())
    Threads.@threads for k = 1:nbr_obs
        th_id = Threads.threadid()
        f_int(x::Float64) = _integrande_ise_univariate_kk(x, observations_scaled[k], bw) 
        integral, err = quadgk(f_int, 0, 1, rtol=1e-5)
        int_kde_square[th_id] += weights[k]^2 * integral
    end
    Threads.@threads for i = 1:nbr_obs
        th_id = Threads.threadid()
        @inbounds for j = (i+1):nbr_obs
            f_int(x::Float64) = _integrande_ise_univariate_ij(x, observations_scaled[i], observations_scaled[j], bw)
            integral, err = quadgk(f_int, 0, 1, rtol=1e-5)
            int_kde_square[th_id] += 2*weights[i]*weights[j] * integral
        end
    end
    int_kde_sq = sum(int_kde_square)
    int_kde_sq /= (upper_bound - lower_bound)

    return int_kde_sq
end
=#
function _ise_gausslegendre_univariate_chen99(observations_scaled::Vector{Float64}, weights::Vector{Float64}, 
                                              lower_bound::Float64, upper_bound::Float64, bw::Float64;
                                              nbr_points_quad::Int = 50)
    nbr_obs = length(observations_scaled)
    int_kde_square = zeros(Threads.nthreads())
    quad_nodes, quad_weights = gausslegendre(nbr_points_quad)
    nthreads = Threads.nthreads()
    l_res_broadcast = Vector{Vector{Float64}}(undef, nthreads)
    for i = 1:nthreads
        l_res_broadcast[i] = zeros(nbr_points_quad)
    end
    Threads.@threads for k = 1:nbr_obs
        th_id = Threads.threadid()
        f_int_rescaled(x::Float64) = _integrande_ise_univariate_kk(0.5+x/2, observations_scaled[k], bw)
        #integral = 0.5 * dot(quad_weights, f_int_rescaled.(quad_nodes))
        broadcast!(f_int_rescaled, l_res_broadcast[Threads.threadid()], quad_nodes)
        integral = 0.5 * dot(quad_weights, l_res_broadcast[Threads.threadid()])
        int_kde_square[th_id] += weights[k]^2 * integral
    end
    Threads.@threads for i = 1:nbr_obs
        th_id = Threads.threadid()
        @inbounds for j = (i+1):nbr_obs
            f_int_rescaled(x::Float64) = _integrande_ise_univariate_ij(0.5+x/2, observations_scaled[i], observations_scaled[j], bw)
            #integral = 0.5 * dot(quad_weights, f_int_rescaled.(quad_nodes))
            broadcast!(f_int_rescaled, l_res_broadcast[Threads.threadid()], quad_nodes)
            integral = 0.5 * dot(quad_weights, l_res_broadcast[Threads.threadid()])
            int_kde_square[th_id] += 2*weights[i]*weights[j] * integral
        end
    end
    int_kde_sq = sum(int_kde_square)
    int_kde_sq /= (upper_bound - lower_bound)

    return int_kde_sq
end
# Estimation of integral of \hat{π}_h^2 by analytical computation
gaussian_convolution(X_i::Float64, X_j::Float64, h::Float64) = 
    (1/(2*h*sqrt(pi)))*exp(-(X_i-X_j)^2/(4*h^2))
function _ise_univariate_gaussian(observations::Vector{Float64}, weights::Vector{Float64}, bw::Float64) 
    nbr_obs = length(observations)
    int_kde_square = zeros(Threads.nthreads())
    Threads.@threads for k = 1:nbr_obs
        th_id = Threads.threadid()
        int_kde_square[th_id] += weights[k]^2 * (1/(2*bw*sqrt(pi)))
    end
    Threads.@threads for i = 1:nbr_obs
        th_id = Threads.threadid()
        @inbounds for j = (i+1):nbr_obs
            int_kde_square[th_id] += 2*weights[i]*weights[j] * gaussian_convolution(observations[i], observations[j], bw)
        end
    end
    int_kde_sq = sum(int_kde_square)

    return int_kde_sq
end
    
# Bandwidth selection
function select_bandwidth(d::UnivariateKDE, l_bound::Float64, u_bound::Float64, nbr_points::Int; 
                          verbose = false, flag_save_results = false, path_results = "./")
    if nprocs() == 1 
        return _select_bandwidth(d, l_bound, u_bound, nbr_points; verbose = verbose,
                                 flag_save_results = flag_save_results, path_results = path_results)
    else
        return _select_bandwidth_distributed(d, l_bound, u_bound, nbr_points; verbose = verbose,
                                             flag_save_results = flag_save_results, path_results = path_results)
    end
end
function _select_bandwidth_distributed(d::UnivariateKDE, l_bound::Float64, u_bound::Float64, nbr_points::Int; 
                                       verbose = false, flag_save_results = false, path_results = "./")
    bw_ref = asymptotic_bandwidth(d)
    bw_lower_bound = (l_bound*bw_ref)
    bw_upper_bound = (u_bound*bw_ref)
    l_bw = bw_lower_bound:((bw_upper_bound-bw_lower_bound)/(nbr_points-1)):bw_upper_bound
    min_bw_lscv = @distributed (min) for i = eachindex(l_bw)
        (lscv(d, l_bw[i]; verbose = verbose), i)
    end
    bw_min = l_bw[min_bw_lscv[2]]
    return bw_min
end
function _select_bandwidth(d::UnivariateKDE, l_bound::Float64, u_bound::Float64, nbr_points::Int; 
                           verbose = false, flag_save_results = false, path_results = "./")
    if flag_save_results 
        if !isdir(path_results) 
            mkdir(path_results) 
        end
    end
    bw_ref = asymptotic_bandwidth(d)
    lscv_min = Inf
    bw_min = 0.0
    a = (l_bound*bw_ref)
    b = (u_bound*bw_ref)
    l_bw = a:((b-a)/(nbr_points-1)):b
    l_lscv = zeros(nbr_points)
    for i = eachindex(l_bw)
        bw = l_bw[i]
        lscv_current = lscv(d, bw; verbose = verbose) 
        l_lscv[i] = lscv_current
        if (lscv_current <= lscv_min) 
            bw_min = bw 
            lscv_min = lscv_current
        end
    end
    if flag_save_results
        plot_and_save(collect(l_bw), l_lscv, bw_min, bw_ref, d.nbr_obs, l_bound, u_bound, path_results)
    end
    return bw_min
end
function plot_and_save(l_bw::Vector{Float64}, l_lscv::Vector{Float64}, 
                       bw_min::Float64, bw_ref::Float64, nbr_obs::Int,
                       l_bound::Float64, u_bound::Float64, path_results::String)
    coefft_bw = bw_min / bw_ref
    @show coefft_bw, bw_min, minimum(l_lscv)
    p = plot(title = "coeff min = $coefft_bw ; bw_min = $bw_min\n nbr_obs = $(nbr_obs)", dpi = 480)
    plot!(p, l_bw, l_lscv, color = "red", line = (1, :dot))
    scatter!(p, l_bw, l_lscv, color = "red", m = (2, :xcross))
    png(p, path_results * "lscv.png")
    nbr_points = length(l_bw)
    mat_bw = zeros(nbr_points, 2)
    mat_bw[:,1] = convert(Array, l_bw)
    mat_bw[:,2] = convert(Array, l_bound:((u_bound-l_bound)/(nbr_points-1)):u_bound)
    writedlm(path_results * "bw.csv", mat_bw, ',')
    writedlm(path_results * "lscv.csv", l_lscv, ',')
end

function minimize_lscv(d::UnivariateKDE, l_bound::Float64, u_bound::Float64; 
                       verbose = false, method::Symbol = :goldensection,
                       maxevals_int::Int = typemax(Int), rel_tol = 1e-6)
    optim_methods = Dict(:goldensection => Optim.GoldenSection(), :brent => Optim.Brent())
    @assert method in keys(optim_methods) "Unrecognized optimization method"
    bw_ref = asymptotic_bandwidth(d)
    lscv_obj(bw) = lscv(d, bw; maxevals = maxevals_int)
    res_optim = Optim.optimize(lscv_obj, l_bound*bw_ref, u_bound*bw_ref, optim_methods[method], rel_tol = rel_tol)
    
    return Optim.minimizer(res_optim)
end

## Multivariate distributions

# Least-square cross validation
function lscv(d::MultivariateKDE, bw::Vector{Float64}; verbose::Bool = false, maxevals::Int = 1000)
    res_ise = ise_kde(d, bw; maxevals = maxevals) 
    res_loo = loo_kde(d, bw)
    res_lscv = res_ise - res_loo
    if verbose
        println("Bandwidth: $bw, LSCV: $res_lscv, square integral kde: $res_ise, loo: $res_loo")
    end
    return res_lscv 
end

# Leave-one out cv estimator
# Estimation of 2 * integral of \hat{π}_h * π  by leave-one out CV
function loo_kde(d::MultivariateKDE, bw::Vector{Float64})
    res_loo = 0.0
    for i = 1:d.nbr_obs
        res_loo += summand_loo_kde(d, i, bw)
    end
    return (2/d.nbr_obs)*res_loo
end
# Summand of leave-one-out estimator
# Compute \hat{π}^{(-i)}_h (x_i)
function summand_loo_kde(d::MultivariateKDE, i::Int, bw::Vector{Float64})
    if d.kernel == "chen99"
        return _summand_loo_kde_multivariate_chen99(d.observations_scaled, d.weights, 
                                                    d.lower_bound, d.upper_bound, i, bw)
    elseif d.kernel == "gaussian"
        return _summand_loo_kde_multivariate_gaussian(d.observations_scaled, d.weights, i, bw)
    else
        return missing
    end
end
#=
function summand_loo_kde(d::MultivariateKDE, i::Int, bw::Vector{Float64})
    if d.kernel == "chen99"
        func_num_res = beta_integrande
    elseif d.kernel == "gaussian"
        func_num_res = gaussian_kernel
    else
        return missing
    end
    l_res = zeros(Threads.nthreads())
    dim = size(d.observations)[1]
    l_res = zeros(Threads.nthreads())
    l_norm_weights = zeros(Threads.nthreads())
    Threads.@threads for j = 1:d.nbr_obs
        if (j == i) continue end
        th_id = Threads.threadid()
        for s=1:dim
            l_res[th_id] += d.weights[j] * func_num_res(d.observations_scaled[s,j], 
                                                           d.observations_scaled[s,i], bw[s])
        end
        l_norm_weights[th_id] += d.weights[j]
    end
    norm_weights = sum(l_norm_weights)
    res = sum(l_res)
    
    cte_denom = 0.0
    if d.kernel == "chen99"
        cte_denom = prod(d.upper_bound - d.lower_bound)
        for s = 1:dim
            cte_denom *= beta(d.observations_scaled[s,i]/bw[s] + 1, (1-d.observations_scaled[s,i])/bw[s] + 1)
        end
        cte_denom *= norm_weights
    elseif d.kernel == "gaussian"
        cte_denom = prod(bw)*norm_weights
    else 
        return missing
    end
    return res / cte_denom
end
=#
function _summand_loo_kde_multivariate_chen99(observations_scaled::Matrix{Float64}, weights::Vector{Float64}, 
                                              lower_bound::Vector{Float64}, upper_bound::Vector{Float64}, 
                                              i::Int, bw::Vector{Float64})
    l_res = zeros(Threads.nthreads())
    dim_obs = size(observations_scaled)[1]
    nbr_obs = size(observations_scaled)[2]
    l_res = zeros(Threads.nthreads())
    l_norm_weights = zeros(Threads.nthreads())
    Threads.@threads for j = 1:nbr_obs
        if (j == i) continue end
        th_id = Threads.threadid()
        #=
        kernel_product = 1.0
        for s = 1:dim_obs
            kernel_product *= beta_integrande(observations_scaled[s,j],  observations_scaled[s,i], bw[s])
        end
        =#
        kernel_product = product_beta_integrande(@view(observations_scaled[:,j]), @view(observations_scaled[:,i]), bw)
        l_res[th_id] += weights[j] * kernel_product
        #l_res[th_id] += weights[j] * beta_integrande(observations_scaled[s,j],  observations_scaled[s,i], bw[s])
        l_norm_weights[th_id] += weights[j]
    end
    norm_weights = sum(l_norm_weights)
    res = sum(l_res)
    cte_denom = norm_weights * prod(upper_bound - lower_bound)
    for s = 1:dim_obs
        cte_denom *= beta(observations_scaled[s,i]/bw[s] + 1, (1-observations_scaled[s,i])/bw[s] + 1)
    end
    return res / cte_denom
end
function _summand_loo_kde_multivariate_gaussian(observations::Matrix{Float64}, weights::Vector{Float64}, 
                                                i::Int, bw::Vector{Float64})
    l_res = zeros(Threads.nthreads())
    dim_obs = size(observations)[1]
    nbr_obs = size(observations)[2]
    l_res = zeros(Threads.nthreads())
    l_norm_weights = zeros(Threads.nthreads())
    Threads.@threads for j = 1:nbr_obs
        if (j == i) continue end
        th_id = Threads.threadid()
        kernel_product = 1.0
        for s = 1:dim_obs
            kernel_product *= gaussian_kernel(observations[s,j], observations[s,i], bw[s])
        end
        l_res[th_id] += weights[j] * kernel_product 
        l_norm_weights[th_id] += weights[j]
    end
    norm_weights = sum(l_norm_weights)
    res = sum(l_res)
    
    cte_denom = prod(bw)*norm_weights
    return res / cte_denom
end

# Integral square error
function ise_kde(d::MultivariateKDE, bw::Vector{Float64}; maxevals::Int = typemax(Int))
    if d.kernel == "chen99"
        return _ise_hcubature_multivariate_chen99(d.log_observations_scaled, d.log_1_observations_scaled, 
                                                  d.weights, d.lower_bound, d.upper_bound, bw; maxevals = maxevals)
    elseif d.kernel == "gaussian"
        return _ise_multivariate_gaussian(d.observations, d.weights, bw)
    else
        nes
        return missing
    end
end
function _integrande_ise_paired_multivariate(x::AbstractVector{Float64}, 
                                             log_obs_scaled::Matrix{Float64}, log_1_obs_scaled::Matrix{Float64}, 
                                             weights::Vector{Float64}, bw::Vector{Float64})
    res = 0.0
    a, b = x ./ bw, (1 .- x) ./ bw
    for k = eachindex(weights)
        @inbounds res += (weights[k] * product_beta_integrande(a, b, @view(log_obs_scaled[:,k]), @view(log_1_obs_scaled[:,k])))^2
    end
    prod_beta = 1.0
    for s = eachindex(bw)
        @inbounds prod_beta *= beta(x[s]/bw[s] + 1, (1-x[s])/bw[s] + 1)^2
    end
    res /= prod_beta
    return res
end
function _integrande_ise_unpaired_multivariate(x::AbstractVector{Float64}, 
                                               log_obs_scaled::Matrix{Float64}, log_1_obs_scaled::Matrix{Float64}, 
                                               weights::Vector{Float64}, bw::Vector{Float64})
    res = 0.0
    a, b = x ./ bw, (1 .- x) ./ bw
    for i = eachindex(weights)
        l_res_i = zeros(Threads.nthreads())
        Threads.@threads for j = (i+1):length(weights)
            @inbounds l_res_i[Threads.threadid()] += weights[j] * product_beta_integrande(a, b, 
                                                                  @view(log_obs_scaled[:,j]), @view(log_1_obs_scaled[:,j]))
        end
        res_i = sum(l_res_i)
        @inbounds res_i *= 2 * weights[i] * product_beta_integrande(a, b, @view(log_obs_scaled[:,i]), @view(log_1_obs_scaled[:,i]))
        res += res_i
    end
    prod_beta = 1.0
    for s = eachindex(bw)
        @inbounds prod_beta *= beta(a[s]+1, b[s]+1)^2
    end
    res /= prod_beta
    return res
end 
# Estimation of integral of \hat{π}_h^2 by numerical integration
function _ise_hcubature_multivariate_chen99(log_obs_scaled::Matrix{Float64}, log_1_obs_scaled::Matrix{Float64}, 
                                            weights::Vector{Float64},
                                            lower_bound::Vector{Float64}, upper_bound::Vector{Float64}, bw::Vector{Float64};
                                            maxevals::Int = typemax(Int))
    dim_obs = size(log_obs_scaled)[1]
    f_int_paired(x) = _integrande_ise_paired_multivariate(x, log_obs_scaled, log_1_obs_scaled, weights, bw)
    f_int_unpaired(x) = _integrande_ise_unpaired_multivariate(x, log_obs_scaled, log_1_obs_scaled, weights, bw)
    #integral, err = hcubature(x -> f_int_paired(x) + f_int_unpaired(x), zeros(dim_obs), ones(dim_obs), rtol=1e-5)
    #int_kde_sq = integral
    integral_paired, err_paired = hcubature(f_int_paired, zeros(dim_obs), ones(dim_obs), rtol=1e-6, maxevals = maxevals)
    integral_unpaired, err_unpaired = hcubature(f_int_unpaired, zeros(dim_obs), ones(dim_obs), rtol=1e-6, maxevals = maxevals)
    int_kde_sq = integral_paired + integral_unpaired
    
    int_kde_sq /= prod(upper_bound - lower_bound)

    return int_kde_sq
end
#=
function _ise_hcubature_multivariate_chen99_bis(observations_scaled::Matrix{Float64}, weights::Vector{Float64}, 
                                                lower_bound::Vector{Float64}, upper_bound::Vector{Float64}, bw::Vector{Float64})
    nbr_obs = length(weights)
    int_kde_square = zeros(Threads.nthreads())
    dim_obs = size(observations_scaled)[1]
    Threads.@threads for k = 1:nbr_obs
        th_id = Threads.threadid()
        f_int(x) = _integrande_ise_multivariate_kk(x, @view(observations_scaled[:, k]), bw)
        integral, err = hcubature(f_int, zeros(dim_obs), ones(dim_obs), rtol=1e-5)
        int_kde_square[th_id] += weights[k]^2 * integral
    end
    Threads.@threads for i = 1:nbr_obs
        th_id = Threads.threadid()
        t(inbounds for j = (i+1):nbr_obs
            f_int(x) = _integrande_ise_multivariate_ij(x, @view(observations_scaled[:,i]), @view(observations_scaled[:,j]), bw)
            integral, err = hcubature(f_int, zeros(dim_obs), ones(dim_obs), rtol=1e-5)
            int_kde_square[th_id] += 2*weights[i]*weights[j] * integral
        end
    end
    int_kde_sq = sum(int_kde_square)
    int_kde_sq /= prod(upper_bound - lower_bound)

    return int_kde_sq
end
=#
# Estimation of integral of \hat{π}_h^2 by analytical computation
function gaussian_convolution(X_i::Vector{Float64}, X_j::Vector{Float64}, h::Vector{Float64})
    dim_obs = length(X_i)
    sigma2_prod = 1/(2*sum(h.^-2))
    mu_prod = 0.0
    for s = 1:dim_obs
        mu_prod += (X_i[s] + X_j[s])/(h[s])^2
    end
    mu_prod *= sigma2_prod
    res = (2*pi)^(-(2*dim_obs-1)/2)
    res *= sqrt(sigma2_prod / prod(h.^4))
    in_exp = 0.0
    for s = 1:dim_obs
        in_exp += (X_i[s] + X_j[s])^2/(h[s])^2
    end
    in_exp -= mu_prod^2 / sigma2_prod
    in_exp *= -0.5
    res *= exp(in_exp)
    return res
end
function _ise_multivariate_gaussian(observations::Matrix{Float64}, weights::Vector{Float64}, 
                                    lower_bound::Vector{Float64}, upper_bound::Vector{Float64}, bw::Vector{Float64};
                                    nbr_points_quad::Int = 50)
    int_kde_square = zeros(Threads.nthreads())
    nbr_obs = size(observations)[2]
    Threads.@threads for k = 1:nbr_obs
        th_id = Threads.threadid()
        int_kde_square[th_id] += weights[k]^2 * gaussian_convolution(observations[:,k], observations[:,k], bw)
    end
    Threads.@threads for i = 1:nbr_obs
        th_id = Threads.threadid()
        @inbounds for j = (i+1):nbr_obs
            int_kde_square[th_id] += 2*weights[i]*weights[j] * gaussian_convolution(observations[:,i], observations[:,j], bw)
        end
    end
    int_kde_sq = sum(int_kde_square)

    return int_kde_sq
end
#=
function lscv(d::MultivariateKDE, bw::Vector{Float64})
    if d.kernel == "chen99"
        # Estimation of integral of \hat{π}_h^2 by numerical integration
        int_kde_square = zeros(Threads.nthreads())
        dims = size(d.observations)[1]
        Threads.@threads for k = 1:d.nbr_obs
            th_id = Threads.threadid()
            integral, err = hcubature(x -> int_estim_square(x, d, bw, k, k), 
                                   zeros(dims), ones(dims), rtol=1e-5)
            int_kde_square[th_id] += d.weights[k]^2 * integral
        end
        Threads.@threads for i = 1:d.nbr_obs
            th_id = Threads.threadid()
            @inbounds for j = (i+1):d.nbr_obs
                integral, err = hcubature(x -> int_estim_square(x, d, bw, i, j),  
                                       zeros(dims), ones(dims), rtol=1e-5)
                int_kde_square[th_id] += 2*d.weights[i]*d.weights[j] * integral
            end
        end
        int_kde_sq = sum(int_kde_square)
        int_kde_sq /= prod(d.upper_bound - d.lower_bound)
        # Estimation of 2 * integral of \hat{π}_h * π  by leave-one out CV
        res_loo = loo_kde(d, bw)
        
        return (int_kde_sq - res_loo) 
    elseif d.kernel == "gaussian"
        # Estimation of integral of \hat{π}_h^2 by analytical computation
        int_kde_square = zeros(Threads.nthreads())
        dims = size(d.observations)[1]
        Threads.@threads for k = 1:d.nbr_obs
            th_id = Threads.threadid()
            int_kde_square[th_id] += d.weights[k]^2 * gaussian_convolution(d.observations[:,k], d.observations[:,k], bw)
        end
        Threads.@threads for i = 1:d.nbr_obs
            th_id = Threads.threadid()
            @inbounds for j = (i+1):d.nbr_obs
                int_kde_square[th_id] += 2*d.weights[i]*d.weights[j] * gaussian_convolution(d.observations[:,i], d.observations[:,j], bw)
            end
        end
        int_kde_sq = sum(int_kde_square)
        # Estimation of 2 * integral of \hat{π}_h * π  by leave-one out CV
        res_loo = loo_kde(d, bw)
         
        return (int_kde_sq - res_loo) 
    else
        return missing
    end
end
=#

# Bandwidth selection
function select_bandwidth(d::MultivariateKDE, l_bound::Vector{Float64}, u_bound::Vector{Float64}, nbr_points::Vector{Int}; 
                          flag_save_results = false, path_results = "./", maxevals_int::Int = typemax(Int))
    if nprocs() == 1 
        return _select_bandwidth(d, l_bound, u_bound, nbr_points; 
                                 flag_save_results = flag_save_results, path_results = path_results, maxevals_int = maxevals_int)
    else
        return _select_bandwidth_distributed(d, l_bound, u_bound, nbr_points; 
                                             flag_save_results = flag_save_results, path_results = path_results, maxevals_int = maxevals_int)
    end
end
function _select_bandwidth_distributed(d::MultivariateKDE, l_bound::Vector{Float64}, u_bound::Vector{Float64}, nbr_points::Vector{Int}; 
                                       flag_save_results = false, path_results = "./", maxevals_int::Int = typemax(Int))
    dim_obs = size(d.observations)[1]
    bw_ref = asymptotic_bandwidth(d)
    @assert length(l_bound) == length(u_bound) == dim_obs "Dimensions doesn't match (l_bound/u_bound/dim of observations)"
    list_iterator_1d_bw = []
    for i = 1:dim_obs
        bw_lower_bound = (l_bound[i]*bw_ref)
        bw_upper_bound = (u_bound[i]*bw_ref)
        l_bw = bw_lower_bound:((bw_upper_bound-bw_lower_bound)/(nbr_points[i]-1)):bw_upper_bound
        push!(list_iterator_1d_bw, l_bw)
    end
    multidim_iterator_bw = Iterators.product(list_iterator_1d_bw...)
    min_bw_lscv = @distributed (min) for tuple_bw in collect(multidim_iterator_bw)
        bw = collect(tuple_bw)
        @show bw
        (lscv(d, bw; maxevals = maxevals_int), bw)
    end
    @show min_bw_lscv
    bw_min = min_bw_lscv[2]
    return bw_min
end
function _select_bandwidth(d::MultivariateKDE, l_bound::Vector{Float64}, u_bound::Vector{Float64}, nbr_points::Vector{Int64}; 
                           flag_save_results = false, path_results = "./", maxevals_int::Int = typemax(Int))
    dim_obs = size(d.observations)[1]
    @assert dim_obs == 2 "select_bandwidth not supported for dim_obs > 2"
    bw_ref = asymptotic_bandwidth(d)
    lscv_min = Inf
    bw_min = 0.0
    a = (l_bound*bw_ref)
    b = (u_bound*bw_ref)
    step_1 = ((b[1]-a[1])/(nbr_points[1]-1))
    step_2 = ((b[2]-a[2])/(nbr_points[2]-1))
    tuple_pts = (nbr_points[1], nbr_points[2])
    l_lscv = zeros(tuple_pts)
    l_x = zeros(tuple_pts)
    l_y = zeros(tuple_pts)
    for i = 1:nbr_points[1]
        for j = 1:nbr_points[2]
            x = a[1] + (i-1)*step_1
            y = a[2] + (j-1)*step_2
            bw = [x, y]
            l_x[i, j] = x
            l_y[i, j] = y
            println("$bw ...")
            lscv_current = lscv(d, bw; maxevals = maxevals_int) 
            l_lscv[i, j] = lscv_current
            if (lscv_current <= lscv_min) 
                bw_min = bw 
                lscv_min = lscv_current
            end
        end
    end
    if flag_save_results
        if !isdir(path_results) mkdir(path_results) end
        fig = plt.figure()
        plt.title("bw_min = $(bw_min)")
        ax = fig.gca(projection="3d")
        surf = ax.plot_surface(l_x, l_y, l_lscv, cmap=cm.coolwarm, linewidth=0, antialiased=false)
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        fig.colorbar(surf, shrink=0.5, aspect=5)
        plt.savefig(path_results * "lscv.png", dpi=480)
        plt.close()
        writedlm(path_results * "mesh_x.csv", l_x, ',')
        writedlm(path_results * "mesh_y.csv", l_y, ',')
        writedlm(path_results * "lscv.csv", l_lscv, ',')
    end
    return bw_min
end

function minimize_lscv(d::MultivariateKDE, l_bound::Vector{Float64}, u_bound::Vector{Float64}; 
                       coeff_init_bw::Vector{Float64} = (u_bound + l_bound) ./ 2, 
                       nt = 2, ns = 2, rt = 0.9, f_tol = 1e-5, x_tol = 1e-4, 
                       lscv_calls_limit::Int = 1000, maxevals_int::Int = typemax(Int), verbosity::Int = 0)
    bw_ref = asymptotic_bandwidth(d)
    lbound_bw = bw_ref .* l_bound
    ubound_bw = bw_ref .* u_bound
    init_bw = bw_ref .* coeff_init_bw
    lscv_obj(bw) = lscv(d, bw; maxevals = maxevals_int)
    res_optim = Optim.optimize(lscv_obj, lbound_bw, ubound_bw, init_bw, 
                               Optim.SAMIN(nt = nt, ns = ns, rt = rt, f_tol = f_tol, x_tol = x_tol, verbosity = verbosity),
                               Optim.Options(f_calls_limit = lscv_calls_limit))
    
    return Optim.minimizer(res_optim)
end

