
struct UnivariateKDE <: ContinuousUnivariateDistribution
    observations::Vector{Float64}
    observations_scaled::Vector{Float64}
    log_observations_scaled::Vector{Float64}
    log_1_observations_scaled::Vector{Float64}
    nbr_obs::Int
    weights::Vector{Float64}
    bandwidth::Float64
    kernel::String
    lower_bound::Float64
    upper_bound::Float64
end

# Constructors
UnivariateKDE(observations::Vector{Float64}, bandwidth::Float64) = 
UnivariateKDE(observations, zeros(0), zeros(0), zeros(0), length(observations), [1/length(observations) for i=1:length(observations)], bandwidth, "gaussian", -Inf, Inf)
function UnivariateKDE(observations::Vector{Float64}; 
                       weights::Union{Vector{Float64}, Nothing}=nothing, bandwidth::Float64=0.0, 
                       kernel::String="gaussian", lower_bound::Float64=-Inf, upper_bound::Float64=Inf)
    @assert lower_bound < upper_bound
    nbr_obs = length(observations)
    l_w = (weights == nothing) ? [1/nbr_obs for i=1:nbr_obs] : weights
    if kernel == "gaussian"
        b = (bandwidth == 0.0) ? optimal_bandwidth_gaussian_1d(nbr_obs, std(observations)) : bandwidth
        return UnivariateKDE(observations, observations, observations, observations, 
                             nbr_obs, l_w, b, kernel, -Inf, Inf)
    elseif kernel == "chen99"
        b = (bandwidth == 0.0) ? asymptotic_bandwidth_beta(1, nbr_obs) : bandwidth
        l_bound = (lower_bound == -Inf) ? 0.0 : lower_bound
        u_bound = (upper_bound == Inf) ? 1.0 : upper_bound
        obs_scaled = zeros(nbr_obs)
        for i = 1:nbr_obs
            obs_scaled[i] = (observations[i]-l_bound)/(u_bound-l_bound)
        end
        log_obs_scaled = log.(obs_scaled)
        log_1_obs_scaled = log.(1 .- obs_scaled)
        return UnivariateKDE(observations, obs_scaled, log_obs_scaled, log_1_obs_scaled, 
                             nbr_obs, l_w, b, kernel, l_bound, u_bound)
    else 
        return missing
    end
end

struct MultivariateKDE <: ContinuousMultivariateDistribution
    observations::Matrix{Float64} # one column = one sample
    observations_scaled::Matrix{Float64} # one column = one sample
    log_observations_scaled::Matrix{Float64} # one column = one sample
    log_1_observations_scaled::Matrix{Float64} # one column = one sample
    nbr_obs::Int
    weights::Vector{Float64}
    bandwidth::AbstractArray
    kernel::String
    lower_bound::Vector{Float64}
    upper_bound::Vector{Float64}
end

# Constructors
MultivariateKDE(observations::Matrix{Float64}, bandwidth::Float64) = 
                    MultivariateKDE(observations, zeros(0,0), zeros(0,0), zeros(0,0), 
                    size(observations)[2], 
                    (1/size(observations)[2])*ones(size(observations)[2]), 
                    bandwidth*ones(size(observations)[1]), "gaussian",
                    -Inf*ones(size(observations)[1]), Inf*ones(size(observations)[1]))

function MultivariateKDE(observations::Matrix{Float64}; 
                         weights::Union{Vector{Float64}, Nothing}=nothing, bandwidth::Union{Float64, AbstractArray}=0.0, 
                         kernel::String="gaussian", 
                         lower_bound::Union{Float64, Vector{Float64}}=-Inf, 
                         upper_bound::Union{Float64, Vector{Float64}}=Inf)
    dim_obs = size(observations)[1]
    nbr_obs = size(observations)[2]
    l_w = (weights == nothing) ? [1/nbr_obs for i=1:nbr_obs] : weights
    if kernel == "gaussian" || kernel == "gaussian2"
        b = bandwidth
        if isa(bandwidth, Float64)
            b = (bandwidth == 0.0) ? [asymptotic_bandwidth_gaussian_multidim(dim_obs, nbr_obs) for i=1:dim_obs] : [bandwidth for i=1:dim_obs]
        end
        return MultivariateKDE(observations, observations, observations, observations, nbr_obs, l_w, b, kernel, -Inf.*ones(dim_obs), Inf.*ones(dim_obs))
    elseif kernel == "chen99"
        b = bandwidth
        if isa(bandwidth, Float64)
            b = (bandwidth == 0.0) ? [asymptotic_bandwidth_beta(dim_obs, nbr_obs) for i=1:dim_obs] : [bandwidth for i=1:dim_obs]
        end
        l_bound = lower_bound
        if isa(lower_bound, Float64)
            l_bound = (lower_bound == -Inf) ? zeros(dim_obs) : lower_bound*ones(dim_obs)
        end
        u_bound = upper_bound
        if isa(upper_bound, Float64)
            u_bound = (upper_bound == Inf) ? ones(dim_obs) : upper_bound*ones(dim_obs)
        end
        obs_scaled = zeros(dim_obs, nbr_obs)
        for i = 1:nbr_obs
            obs_scaled[:,i] = (observations[:,i]-l_bound)./(u_bound-l_bound)
        end
        log_obs_scaled = log.(obs_scaled)
        log_1_obs_scaled = log.(1 .- obs_scaled)
        return MultivariateKDE(observations, obs_scaled, log_obs_scaled, log_1_obs_scaled, 
                               nbr_obs, l_w, b, kernel, l_bound, u_bound)
    else 
        return missing
    end
end

