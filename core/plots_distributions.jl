
function plot(d::UnivariateKDE; nbr_points::Int = 100)
    step_xaxis = (d.upper_bound - d.lower_bound) / (nbr_points - 1)
    xaxis = d.lower_bound:step_xaxis:d.upper_bound
    d_pdf(x) = pdf(d, x)
    p = plot(title = "Univariate KDE", dpi = 480, background_color_legend = :transparent)
    plot!(p, xaxis, d_pdf.(xaxis))
end

function plot(d::MultivariateKDE; 
              nbr_points = (100,100), xlim = (d.lower_bound[1], d.upper_bound[1]), ylim = (d.lower_bound[2], d.upper_bound[2]))
    @assert size(d.observations)[1] == 2 "Only 2 dimensional KDE for plots"
    
    step_xaxis = (xlim[2] - xlim[1]) / (nbr_points[1] - 1)
    xaxis = xlim[1]:step_xaxis:xlim[2]
    step_yaxis = (ylim[2] - ylim[1]) / (nbr_points[2] - 1)
    yaxis = ylim[1]:step_yaxis:ylim[2]
    d_pdf(x,y) = pdf(d, [x,y])
    p = plot(title = "Multivariate KDE", dpi = 480, background_color_legend = :transparent)
    plot!(p, xaxis, yaxis, d_pdf, st = :surface, c = :coolwarm, camera = (30, 45))
    
    #=
    iterator_grid = Iterators.product(xaxis, yaxis)
    list_x_grid = vec([tuple[1] for tuple in iterator_grid])
    list_y_grid = vec([tuple[2] for tuple in iterator_grid])
    plot!(p, list_x_grid, list_y_grid, d_pdf.(list_x_grid, list_y_grid), st = :surface, c = :coolwarm, camera = (30, 45))
    =#
end

