
gaussian_kernel(x_obs::Float64, mu::Float64, h::Float64) = exp(-0.5*log(2*pi) - (x_obs - mu)^2 / (2*h^2))
beta_integrande(x_obs_scaled::Float64, t::Float64, h::Float64) = x_obs_scaled^(t/h) * (1 - x_obs_scaled)^((1-t)/h)
beta_integrande(a::Float64, b::Float64, log_x::Float64, log_1_x::Float64) = exp(a*log_x + b*log_1_x)
beta_integrande_square(x_obs_scaled::Float64, t::Float64, h::Float64) = x_obs_scaled^(2t/h) * (1 - x_obs_scaled)^(2(1-t)/h)
beta_integrande_product(x_obs_scaled_i::Float64, x_obs_scaled_j::Float64, t::Float64, h::Float64) = 
    (x_obs_scaled_i*x_obs_scaled_j)^(t/h) * ((1-x_obs_scaled_i)*(1-x_obs_scaled_j))^((1-t)/h)
#=
beta_integrande_2(x_obs_scaled::Float64, t::Float64, h::Float64) = exp((t*log(x_obs_scaled) + (1-t)*log(1-x_obs_scaled))/h)
beta_integrande_3(x_obs_scaled::Float64, t::Float64, h::Float64) = exp((t*log(x_obs_scaled/(1-x_obs_scaled)) + log(1-x_obs_scaled))/h)
beta_integrande_4(x_obs_scaled::Float64, t::Float64, h::Float64) = (x_obs_scaled/(1-x_obs_scaled))^(t/h) * (1 - x_obs_scaled)^(1/h)
=#
function product_beta_integrande(x_obs_scaled::SubArray{Float64,1}, x::AbstractVector{Float64}, 
                                 bw::Vector{Float64})
    product = 1.0
    for s = eachindex(bw)
        @inbounds product *= beta_integrande(x_obs_scaled[s], x[s], bw[s])
    end
    return product
end
function product_beta_integrande(a::Vector{Float64}, b::Vector{Float64},
                                 log_obs_scaled::SubArray{Float64,1}, log_1_obs_scaled::SubArray{Float64,1})
    in_exp = 0.0
    for s = eachindex(a)
        @inbounds in_exp += a[s] * log_obs_scaled[s] + b[s] * log_1_obs_scaled[s]
    end
    return exp(in_exp)
end 
#=
function product_beta_integrande_v2(x_obs_scaled::SubArray{Float64,1}, x::AbstractVector{Float64}, 
                                    bw::Vector{Float64})
    return reduce(*, beta_integrande.(x_obs_scaled, x, bw))
end 

function product_beta_integrande_v2(obs_scaled::AbstractMatrix{Float64}, k::Int,
                                    x::AbstractVector{Float64}, bw::AbstractVector{Float64})
    product = 1.0
    for s = eachindex(x)
        @inbounds product *= beta_integrande(obs_scaled[s,k], x[s], bw[s])
    end
    return product
end 
function product_beta_integrande_product_v2(obs_scaled::AbstractMatrix{Float64}, i::Int, j::Int,
                                            x::AbstractVector{Float64}, bw::AbstractVector{Float64})
    product = 1.0
    for s = eachindex(x)
        @inbounds product *= beta_integrande_product(obs_scaled[s,i], obs_scaled[s,j], x[s], bw[s])
    end
    return product
end 
=#
# Univariate
length(d::UnivariateKDE) = 1
rand(gen::AbstractRNG, d::UnivariateKDE) = rand(gen, Normal(d.observations[rand(gen, Categorical(d.weights))], d.bandwidth))
function rand(d::UnivariateKDE)
    if d.kernel == "gaussian"
        return rand(Normal(d.observations[rand(1:(d.nbr_obs))], d.bandwidth))
    elseif d.kernel == "chen99"
        return missing
    end
    return missing
end
sampler(d::UnivariateKDE) = d
pdf(d::UnivariateKDE, x::Real) = exp(logpdf(d, x))
#cdf
#quantile
maximum(d::UnivariateKDE) = d.upper_bound
minimum(d::UnivariateKDE) = d.lower_bound
function mean(d::UnivariateKDE)
    if d.kernel == "gaussian"
        res = 0.0
        for i = 1:d.nbr_obs
            res += d.observations[i]
        end
        return res / d.nbr_obs
    end
    return missing
end
insupport(d::UnivariateKDE, x::Real) = (d.lower_bound <= x <= d.upper_bound)
eltype(d::UnivariateKDE) = Float64
function logpdf(d::UnivariateKDE, x::Real)
    if d.kernel == "gaussian"
        elt_logpdf = 0.0
        for j = 1:d.nbr_obs
            elt_logpdf += d.weights[j] * gaussian_kernel(x, d.observations[j], d.bandwidth) 
        end
        elt_logpdf = -log(d.bandwidth) + log(elt_logpdf)
        return elt_logpdf
    elseif d.kernel == "chen99"
        x_scaled = (x-d.lower_bound)/(d.upper_bound-d.lower_bound)
        elt_logpdf = 0.0
        for j = 1:d.nbr_obs
            obs_scaled = (d.observations[j]-d.lower_bound)/(d.upper_bound-d.lower_bound)
            elt_logpdf += d.weights[j] * beta_integrande(obs_scaled, x_scaled, d.bandwidth)
        end
        elt_logpdf = -log(d.upper_bound - d.lower_bound) -log(beta(x_scaled/d.bandwidth + 1, (1-x_scaled)/d.bandwidth + 1)) + log(elt_logpdf)
        return elt_logpdf
    end
    return missing
end

function asymptotic_bandwidth(d::UnivariateKDE)
    if d.kernel == "chen99"
        bw_ref = asymptotic_bandwidth_beta(1, d.nbr_obs)
    elseif d.kernel == "gaussian"
        bw_ref = optimal_bandwidth_gaussian_1d(d.nbr_obs, std(d.observations))
    else
        return missing
    end
end

# Multivariate

length(d::MultivariateKDE) = size(d.observations)[1]
sampler(d::MultivariateKDE) = d
eltype(d::MultivariateKDE) = Float64
function _rand!(gen::AbstractRNG, d::MultivariateKDE, x::AbstractVector)
    if d.kernel == "gaussian"
        idx = rand(gen, Categorical(d.weights))
        _rand!(gen, MvNormal(d.observations[:, idx], d.bandwidth), x)
    end
end
gaussian_kernel(x::Vector{Float64}, mu::Vector{Float64}, h::Float64) = exp(-(length(x)/2)*log(2*pi) - ((x - mu)'*(x - mu) / (2*h^2)))
gaussian_kernel(x::Vector{Float64}, mu::Vector{Float64}, inv_H::AbstractArray) = 1/(sqrt((2*pi)^length(x))*sqrt((det(inv_H))))*exp(-0.5*(x - mu)'*inv_H*(x - mu))
gaussian_kernel(x::Vector{Float64}) = exp(-(length(x)/2)*log(2*pi) - (x'*x)/2)
# With product of univariate kde
function chen99_multivariate_kde(d::MultivariateKDE, x::AbstractArray)
    if d.kernel == "chen99"
        elt_pdf = 0.0
        dims = size(d.observations)[1]
        x_scaled = (x-d.lower_bound)./(d.upper_bound-d.lower_bound)
        for i = 1:d.nbr_obs
            elt_pdf_obs = d.weights[i]
            for s = 1:dims
                elt_pdf_obs *= beta_integrande(d.observations_scaled[s,i], x_scaled[s], d.bandwidth[s])
            end
            elt_pdf += elt_pdf_obs
        end
        cte_denom = prod(d.upper_bound - d.lower_bound)
        for s = 1:dims
            cte_denom *= beta(x_scaled[s]/d.bandwidth[s] + 1, (1-x_scaled[s])/d.bandwidth[s] + 1) 
        end
        elt_pdf = elt_pdf / cte_denom
        return elt_pdf
    end
    return missing
end
function _logpdf(d::MultivariateKDE, x::AbstractArray)
    if d.kernel == "gaussian"
        elt_logpdf = 0.0
        for j = 1:d.nbr_obs
            elt_logpdf += d.weights[j] * gaussian_kernel((x - d.observations[:,j]) ./ d.bandwidth) 
        end
        elt_logpdf = - log(prod(d.bandwidth)) + log(elt_logpdf) # verif si ya pas sqrt
        return elt_logpdf
    elseif d.kernel == "gaussian2"
        elt_logpdf = 0.0
        inv_H = inv(d.bandwidth)
        for j = 1:d.nbr_obs
            elt_logpdf += d.weights[j] * gaussian_kernel(x, d.observations[:,j], inv_H) 
        end
        return log(elt_logpdf)
    elseif d.kernel == "chen99"
        return log(chen99_multivariate_kde(d, x))
    else
        return missing
    end
    return missing
end

function getproperty(d::MultivariateKDE, sym::Symbol)
    if sym == :dim_obs
        return size(d.observations)[1]
    else
        return getfield(d, sym)
    end
end

function change_bandwidth(d::UnivariateKDE, bw::Float64)
    return UnivariateKDE(d.observations, d.observations_scaled, 
                         d.log_observations_scaled, d.log_1_observations_scaled,
                         d.nbr_obs, d.weights, bw, d.kernel,
                         d.lower_bound, d.upper_bound)
end

function change_bandwidth(d::MultivariateKDE, bw::AbstractArray)
    return MultivariateKDE(d.observations, d.observations_scaled, 
                           d.log_observations_scaled, d.log_1_observations_scaled,
                           d.nbr_obs, d.weights, bw, d.kernel,
                           d.lower_bound, d.upper_bound)
end

function asymptotic_bandwidth(d::MultivariateKDE)
    if d.kernel == "chen99"
        bw_ref = asymptotic_bandwidth_beta(d.dim_obs, d.nbr_obs)
    elseif d.kernel == "gaussian"
        bw_ref = asymptotic_bandwidth_gaussian_multidim(d.dim_obs, d.nbr_obs)
    else
        return missing
    end
end

