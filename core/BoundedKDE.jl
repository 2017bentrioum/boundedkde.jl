
module BoundedKDE

import Optim
import LinearAlgebra: inv, det, dot
import DelimitedFiles: writedlm
import Distributed: @distributed, nprocs, nworkers
import SpecialFunctions: beta
import Base: rand, length, maximum, minimum, getproperty
import Statistics: mean, std
import StaticArrays: reinterpret, SVector
import Random: AbstractRNG
import Distributions: ContinuousUnivariateDistribution, ContinuousMultivariateDistribution 
import Distributions: Normal, MvNormal, Categorical
import Distributions: pdf, logpdf, _logpdf, _rand!
import QuadGK: quadgk
import FastGaussQuadrature: gausslegendre
import HCubature: hcubature
import Plots: plot, plot!, scatter!, surface!, png
#=
using PyCall
pa = pyimport("matplotlib.patches")
cm = pyimport("matplotlib.cm")
mplot3d = pyimport("mpl_toolkits.mplot3d")
=#

export UnivariateKDE, MultivariateKDE
export change_bandwidth
export length, rand, sampler, pdf, logpdf, maximum, minimum, mean, insupport, eltype, asymptotic_bandwidth
export optimal_bandwidth_gaussian_1d, asymptotic_bandwidth_gaussian_multidim, asymptotic_bandwidth_beta
export summand_loo_kde, loo_kde, ise_kde, lscv,  select_bandwidth, minimize_lscv
export gaussian_kernel, beta_integrande
export get_module_path, load_plots_distributions
export plot

include("common.jl")
include("utils_distributions.jl")
include("plots_distributions.jl")
include("bandwidth_selection.jl")
include("utils_pkg.jl")

end

